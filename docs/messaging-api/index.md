# Messaging API

Messages are used to notify the workers of changes without reloading them. This is more efficient than spawning workers.

Messages can be sent programmatically, using the `.message(...)` method. Messages are also sent by the CLI (i.e. `reload`, `watch`, `stop` commands) and the edge agent which receives them from the core api service.

All messages require at least the `type`, `domain`, and `root` properties.

- `type` identifies what kind of information is described. The supported types are defined below.
- `domain` is the host name of the site to which this message applies.
- `root` is the path of base directory containing the site's files.

Additionally, the `configuration` field contains a valid [configuration](#configuration) document. Only the host options matching the `domain` are applied, others are ignored. The `manifest` property can not reference an external file; only an array of inline server push rules is allowed.

For example:

```js
{
  type: 'site-deploy',
  domain: 'example.net',
  root: '/var/www/html',
  configuration: [{
    domain: 'example.net',
    fallback: {200: '/index.html'}
  }]
}
```

## Type: `site-deploy`

- `isNewDomain` Set to `true` if this domain name was not previously served. Otherwise `false`. Not currently used.
- `hasNewFiles` Set to `true` if any files are changed, added, or removed. Otherwise `false`.
- `hasNewConfiguration` Set to true if any option in the site configuration is modified.

```js
{
  type: 'site-deploy',
  domain: 'example.net',
  isNewDomain, // boolean
  hasNewFiles, // boolean
  hasNewConfiguration // boolean
}
```

## Type: `site-delete`

A site has been removed. Clear any cached information for the domain.

```js
{
  type: 'site-delete',
  domain: 'example.net'
}
```

## Type: `configuration-update`

New configuration options are used for the domain.

```js
{
  type: 'configuration-update',
  domain: 'example.net'
}
```

## Type: `file-delete`

One or more files have been removed. Clear their cached information.

```js
{
  type: 'file-delete',
  domain: 'example.net',
  files: [
    {path: 'foo/bar.lol'},
    {path: 'abc/def/ghi.jkl'},
    {path: '123.xyz'}
  ]
}
```

## Type: `file-update`

One or more files have been changed. Reset or reload their cached information.

```js
{
  type: 'file-update',
  domain: 'example.net',
  files: [
    {path: 'foo/bar.lol'},
    {path: 'abc/def/ghi.jkl'},
    {path: '123.xyz'}
  ]
}
```

## Type: `certificate-issue`

A new certificate for the domain is available. Clear any previously cached certificate.

```js
{
  type: 'certificate-issue',
  domain: 'example.net'
}
```

## Type: `certificate-revoke`

The previous certificate for the domain is no longer in use. Clear any cached information.

```js
{
  type: 'certificate-revoke',
  domain: 'example.net'
}
```
