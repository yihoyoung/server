# JavaScript API

## Constructor

```js
const {Master} = require('@commonshost/server')
const server = new Master(configuration)
```

The `configuration` argument contains:

- `options`: The [server configuration](#configuration) object.
- `generateCertificate`: Defaults to `false`. If `true`, a self-signed, trusted certificate for `DNS:localhost` is created on startup in case `https.key` and `https.cert` are missing.
- `serveDefaultSite`: Defaults to `true`. Creates a fallback host configuration using default settings if no `hosts` are explicitly configured. This is a convenience feature for CLI usage so that no configuration is needed by default.
- `cwd`: Defaults to the current working directory (`./`). File and directory paths in the configuration are relative to this path.
- `configurationFilepath`: Defaults to `undefined`. Set this to a file path to enable external server push manifests. External manifests are resolved relative to this file path.

## Starting

```js
await server.listen()
```

Loads the configuration and spawns worker processes to listen for incoming connections. Also starts a redirecting server from HTTP to HTTPS.

## Stopping

```js
await server.close()
```

Shuts down the workers and redirecting server gracefully.

## Reloading

```js
await server.reload()
```

Starts new workers to gracefully replace the old ones without downtime. Reloads the configuration.

## Run-Time Updates

```js
await server.message(message)
```

Invalidates cached information about files, certificates, and configuration on a per-domain level. Used to update at run-time without the performance cost of a full reload. See [Messaging](#messaging) for details.
