# Host Options

## `hosts[].domain`

Default: `''` or `'localhost'` (only for the default site, see [serveDefaultSite](#constructor))

The DNS hostname of the site. May be specified as an Internationalized Domain Name (IDN) containing non-ASCII characters.

Examples:

```js
{
  hosts: [
    { domain: 'example.net' },
    { domain: 'xn--yfro4i67o.example.net' },
    { domain: '🦄.example.net' }
  ]
}
```

## `hosts[].root`

The path to the base directory containing static files to serve.

If no root is specified, the server tries to auto-detect static site generator or packaging tool output directories. For example: `./dist`, `./public`, `./_site`, and many more.

If no directory is auto-detected, the current working directory is used. A warning message is logged to indicate this fallback behaviour.

## `hosts[].fallback`

Default: `{}` (no fallback)

An object mapping HTTP status code to file paths.

Fallbacks are supported for:

- `200` to serve a missing file as a success response. Typically used for client side routing.
- `404` to serve a missing file as an error response. Typically used for client side routing.

Example:

```js
{
  fallback: {
    200: './index.html'
  }
}
```

## `hosts[].cacheControl`

Sets the HTTP caching headers.

### `hosts[].cacheControl.immutable`

Default: `[]`

An array of path [globs](https://www.npmjs.com/package/micromatch) for immutable files.

Special named patterns can be used as shorthand. These are:

- `hex` — Matches hexadecimal hash revved files. Example: `layout-d41d8cd98f.css`
- `emoji` — Matches emoji revved files. Example: `app.⚽️.js`

By default, all non-immutable responses have the header:

```
cache-control: public, max-age=1, must-revalidate
```

File paths that match the patterns set by the `cacheControl.immutable` option are considered to *never, ever* change their contents. To tell browsers never to revalidate these resources, they are served with the header:

```
cache-control: public, max-age=31536000, immutable
```

Examples:

```js
{
  cacheControl: {
    immutable: [
      '/library/v1.2.3/**/*.js'
    ]
  }
}
```

## `hosts[].directories`

Behaviour of directory paths.

### `hosts[].directories.trailingSlash`

Default: `'always'`

Enforces a consistent *clean URL* for directory paths.

The value is a string that can be:
- `'always'` to use HTTP redirects to append a `/` at the end of the directory name.
- `'never'` to use HTTP redirects to strip any `/` at the end of the directory name.

## `hosts[].accessControl`

Settings related to CORS.

### `hosts[].accessControl.allowOrigin`

Default: `'*'`

If specified, sets this value as the `access-control-allow-origin` header on every response.

## `hosts[].serviceWorker`

Settings related to service workers.

### `hosts[].serviceWorker.allowed`

Default: `'/'`

If specified, sets this value as the `service-worker-allowed` header on every response.

## `hosts[].strictTransportSecurity`

Settings related to HTTP Strict Transport Security.

### `hosts[].strictTransportSecurity.maxAge`

Default: `5184000` (60 days)

Sets this value as the `strict-transport-security` header on every response.

## `hosts[].manifest`

The `manifest` property declares which files or URLs need to be pushed as dependencies for any request. Using HTTP/2 Server Push (`PUSH_PROMISE` frames) can eliminate round trips between browser and server and speed up resource loading.

The `manifest` property type can be:

- **Array**: *Inline manifest*, see the [HTTP/2 Server Push Manifest](https://www.npmjs.com/package/@commonshost/manifest) specification for details and examples.
- **String**: *File path* of an external manifest. This path is relative to the server configuration file.

Example: Inline manifest

```js
{
  hosts: [
    domain: 'localhost',
    root: './dist',
    manifest: [
      // Example:
      {
        // When serving the homepage,
        get: '**/*.html',
        // push all CSS and JS files.
        push: ['**/*.css', '**/*.js']
      }
    ]
  ]
}
```

Example: External manifest file

Using an external file is useful when using a build tool that traces dependencies to automatically generate the manifest.

This example shows a static website in the `./dist` directory. The first section runs the [`@commonshost/manifest`](https://www.npmjs.com/package/@commonshost/manifest#cli) command line interface (CLI) tool to trace all HTML/JS/CSS files for dependencies. The automatically generated manifest is stored as `./manifest`. The second section shows a reference to this external manifest.

```
$ npx @commonshost/manifest generate ./dist ./manifest.json
```

```js
{
  hosts: [{
    manifest: './manifest.json'
  }]
}
```
