# Server Options

## `signature`

Default: `true`

Boolean value that adds the `Server:` header to all HTTP responses if `true`, and omits that header if `false`.

The value of the header identifies the versions of the server, Node.js, nghttp2, and the operating system.

Often disabled for reasons of paranoia or placebo performance tuning.

## `acme`

Settings related to the [Automated Certificate Management Environment (ACME)](https://datatracker.ietf.org/wg/acme/) protocol, i.e. support for the [LetsEncrypt](https://letsencrypt.org) certificate authority. This affects all requests, across all hosted domains, where the pathname starts with [`/.well-known/acme-challenge/`](https://www.iana.org/assignments/well-known-uris/well-known-uris.xhtml). There are many ways to implement ACME support. These settings offer flexibility in issuing certificates and serving the correct certificate for each host.

When a certificate is requested for a domain using the ACME HTTP challenge, the certificate authority (i.e. LetsEncrypt's [Boulder](https://github.com/letsencrypt/boulder) server) resolves the given DNS hostname and makes an HTTP request. The webserver needs to respond with values only known by the entity that requested the certificate. In a simple case of one webserver this can be handled by serving a local directory through `acme.webroot`. But when DNS records point to multiple edge servers, it is necessary to relay or deflect the challenge request, via `acme.proxy` or `acme.redirect` respectively, to the certificate requesting entity.

Any domains that do not have a certificate, as per `acme.key` and `acme.cert` lookups, are served using the fallback certificate in `https.key` and `https.cert`.

### `acme.proxy`

Default: `''`

If a valid URI is specified, the server proxies all ACME challenge requests to this upstream server.

Proxying requires the server to do more work than a redirect, but it is transparent to the CA. This allows use of ports other than `80` and `443`, to which Boulder is restricted.

This string is prepended to the challenge request's URL path, so omit any trailing shash to avoid duplication. Both HTTP and HTTPS can be used, depending on the scheme (`http:` vs `https:`).

Example:

```js
{
  acme: {
    proxy: 'https://vault.example.net:8443'
  }
}
```

### `acme.redirect`

Default: `''`

If a valid URI is specified, the server redirects, through a [`301 Permanent Redirect`](https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml) response, all ACME challenge requests to this upstream server.

Redirecting is light on the edge server, but requires a second connection from Boulder to the specified server, possibly adding some latency.

Both HTTP and HTTPS can be used, depending on the scheme, i.e. `http:` or `https:` respectively. Note that Boulder, the LetsEncrypt reference server, is currently restricted to ports `80` and `443`. Redirecting to any other ports is not supported, and should be handled through `acme.proxy`.

This string is prepended to the challenge request's URL path. Omit any trailing shash to avoid duplication.

Example:

```js
{
  acme: {
    redirect: 'https://vault.example.net'
  }
}
```

### `acme.webroot`

Default: `''`

If a valid path is specified, the server responds to challenges by mapping to static files in this directory. This is intended to work with the `webroot` option of many ACME clients, where files are generated during the challenge-response process.

```js
{
  acme: {
    webroot: '/var/www/html'
  }
}
```

### `acme.store`

Default: `''`

Directory where keys and certificates are stored.

Example:

```js
{
  acme: {
    webroot: '/etc/ssl'
  }
}
```

### `acme.key`

Default: `'$store/$domain/key.pem'`

Path to the domain-specific key file.

May contain substitution variables `$store` and `$domain`.

Example:

```js
{
  acme: {
    key: '$store/sites/$domain/crypto/key.pem'
  }
}
```

### `acme.cert`

Default: `'$store/$domain/cert.pem'`

Path to the domain-specific certificate file.

May contain substitution variables `$store` and `$domain`.

Example:

```js
{
  acme: {
    cert: '$store/sites/$domain/crypto/cert.pem'
  }
}
```

## `doh`

Default: `false`

Settings for DNS over HTTPS support.

Set to `true` for defaults (local DNS resolver) or an object containing custom configuration.

### `doh.protocol`

Default: `udp4`

Can be either `udp4` or `udp6` to indicate whether to connect to the resolver over IPv4 or IPv6 respectively.

### `doh.localAddress`

Default: `0.0.0.0` (IPv4) or `::0` (IPv6)

The UDP socket is bound to this address.

Use a loopback IP address (`''` empty string, `localhost`, `127.0.0.1`, or `::1`) to only accept local DNS resolver responses.

Use a wildcard IP address (`0.0.0.0` or `::0`) to accept remote DNS resolver responses.

### `doh.resolverAddress`

Default: `127.0.0.1` (IPv4) or `::1` (IPv6)

The IP address of the DNS resolver. Queries are sent via UDP.

See also: [List of public DNS service operators](https://en.wikipedia.org/wiki/Public_recursive_name_server) on Wikipedia.

### `doh.resolverPort`

Default: `53`

The port of the DNS resolver.

### `doh.timeout`

Default: `10000`

Number of milliseconds to wait for a response from the DNS resolver.

## `log`

Settings for the [Pino](https://getpino.io)-based logger.

### `log.level`

Default: `'info'`

Minimum threshold for log messages to be recorded. One of `fatal`, `error`, `warn`, `info`, `debug`, `trace`; or `silent` to disable logging.

## `http`

An object containing settings for the automatic redirection of HTTP to HTTPS. This is necessary since HTTP/2 is only supported by browsers when using a TLS connection (TLS).

### `http.redirect`

Default: `true`

If `true`, an HTTP server listens to redirect requests to HTTPS.

If `false`, no HTTP server is started. This also means ACME is disabled, since it is served over HTTP as well.

### `http.from`

Default: `8080`

The port number where the HTTP server accepts connections.

### `http.to`

Default: `8443`

The port number of the HTTPS URLs to which HTTP traffic is redirected.

Redirects use the `308 Permanent Redirect` status code.

## `https`

Settings for the fallback TLS context that is used if no files match `acme.key` and `acme.cert`.

Useful when serving subdomains using a wildcard certificate.

If no fallback exists on launch, a self-signed certificate and key pair is generated using [tls-keygen](https://www.npmjs.com/package/tls-keygen). The certificate is attempted to be added to to the operating system trusted store, which may require user confirmation and entry of their password. This is only done in CLI mode, or if the `generateCertificate` API argument is `true`; otherwise the server simply fails to launch.

See the corresponding options of [tls.createSecureContext](https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options).

### `https.key`

Default: `'~/.commonshost/key.pem'`

A string of the path to a PEM file containing the secret key.

### `https.cert`

Default: `'~/.commonshost/cert.pem'`

A string of the path to a PEM file containing the public certificate.

### `https.ca`

Default: `[]`

An array of strings containing the path to all files in the certificate chain.

### `https.port`

Default: `8443`

The TCP/IP port number for incoming TLS connections.

The [standard HTTPS port](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=https) is `443` for both HTTP/1.1 and HTTP/2. However, on Unix (Linux/MacOS), root permissions are required to listen on port numbers from 1 to 1024. The default value is outside this restricted range.

## `placeholder`

By default, requests for sites that are not configured under hosts are responded to with a `404` status and `Not Found` error message.

### `placeholder.hostNotFound`

File path to an HTML response to be served when a request is made to an unrecognised hostname. The response status code is `404`.

Only a single file is allowed. Any required assets should either be inlined or hosted on an external domain.

The placeholder is only served if the request accepts an HTML response and the pathname either has no file extension or ends in `.html`.

## `via`

The `Via` HTTP header is used to detect, and break out of, CDN loops. This can happen due to chained CDNs or misconfigured DNS.

### `via.pseudonym`

Default: `''`

By adding a token string to the `Via` HTTP header, intermediary proxies or CDN servers can identify themselves.

The `pseudonym` value should uniquely identify the intermediary. This is operationally dependent, for example in some cases multiple servers may form a single pool.

Example:

```js
via: {
  pseudonym: `${require('os').hostname()} (Commons Host)`
}
```

Response HTTP headers:

```
Via: 1.1 varnish, 2.0 us-nyc-3.commons.host (Commons Host), 1.1 s_bd39 (squid/3.5.23)
```

## `workers`

The server runs one master process that controls one or more worker processes. The workers process incoming requests and generate responses. Communication between the master and workers uses Node.js [cluster](https://nodejs.org/api/cluster.html) module message passing.

### `workers.count`

Default: `'max_physical_cpu_cores'`

The desired number of worker processes to spawn at launch. Must be a number or a [mathematical expression](https://www.npmjs.com/package/expr-eval) that evaluates to a number.

Examples:

Single worker process:

```js
workers: {
  count: 1
}
```

Invalid: must spawn at least 1 worker.

```js
workers: {
  count: 0
}
```

One worker per CPU core:

```js
workers: {
  count: 'max_physical_cpu_cores'
}
```

Half as many workers as CPU cores:

```js
workers: {
  count: 'max_physical_cpu_cores / 2'
}
```

Leave one core for other processes:

```js
workers: {
  count: 'max_physical_cpu_cores - 1'
}
```

## `www`

### `www.redirect`

Default: `false`

Enable to redirect all sites between the `www.` subdomain and root domain (also known as apex, bare, or naked domain).

Example: Given a scenario where only the hosts `example.net` and `www.example.com` are configured on the server.

| `www.redirect` | Request | Response |
|-|-|-|
| *any*   | example.net | `200` OK |
| *any*   | www.example.com | `200` OK |
| `true`  | www.example.net | `308` Redirect to example.net |
| `true`  | example.com | `308` Redirect to www.example.com |
| `false` | www.example.net | `404` Not Found |
| `false` | example.com | `404` Not Found |

## `hosts`

Default: `[]`

An array of objects containing [host options](#host-options) configurations.
