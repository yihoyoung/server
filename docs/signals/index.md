# Signals

If the server is started from the command line, it can be controlled by sending process signals.

## SIGINT

Graceful shutdown of all workers and the master process.

## SIGHUP

Gracefully restart all workers with reloaded configurations.
