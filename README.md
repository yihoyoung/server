# @commonshost/server

Static HTTP/2 webserver for single page apps and progressive web apps.

- HTTP/2, HTTP/1.1, and HTTP/1.0
- Content encoding for Brotli and Gzip compression
- Server Push Manifests & Cache Digest
- Scalable, multi-processor clustering
- Auto-generate HTTPS certificate for localhost development
- Fallback for client side routing
- HTTP to HTTPS redirect
- CORS
- Immutable caching of revved files
