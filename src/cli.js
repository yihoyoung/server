require('hard-rejection/register')
const updateNotifier = require('update-notifier')
const yargs = require('yargs')
const { join } = require('path')
const { fromUrl } = require('hosted-git-info')
const readPkgUp = require('read-pkg-up')

module.exports.cli = async () => {
  const { pkg } = await readPkgUp({ cwd: __dirname })
  process.title = 'commonshost'
  updateNotifier({ pkg }).notify()
  await yargs
    .usage('Usage: $0 <command>')
    .example('$0 --help <command>', 'Show command-specific options')
    .epilogue(`Documentation:\n  ${fromUrl(pkg.repository.url).docs()}`)
    .commandDir(join(__dirname, 'commands'))
    .demandCommand(1)
    .help('help').alias('h', 'help')
    .version().alias('v', 'version')
    .wrap(null)
    .parse(process.argv.slice(2))
}
