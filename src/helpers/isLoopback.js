const { untilBefore } = require('until-before')
const { getHost } = require('../helpers/getHost')

const loopbackRequestCache = new WeakMap()

module.exports.isLoopback = (request) => {
  if (loopbackRequestCache.has(request)) {
    return loopbackRequestCache.get(request)
  }

  const host = untilBefore.call(getHost(request), ':')

  const matchLoopback = host === 'localhost' ||
    host === '127.0.0.1' ||
    host === '::1'

  loopbackRequestCache.set(request, matchLoopback)

  return matchLoopback
}
