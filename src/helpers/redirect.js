const DEFAULT_PORT_HTTPS = 443
const PERMANENT_REDIRECT = 308

module.exports.redirect = (response, hostname, port, pathname) => {
  const host = port === DEFAULT_PORT_HTTPS ? hostname : `${hostname}:${port}`
  const location = `https://${host}${pathname}`
  response.statusCode = PERMANENT_REDIRECT
  response.writeHead(PERMANENT_REDIRECT, { location })
  response.end()
}
