const { statSync } = require('fs')

module.exports.directoryExists = (path) => {
  try {
    const stats = statSync(path)
    return stats.isDirectory()
  } catch (err) {
    return false
  }
}
