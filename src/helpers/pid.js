const { join, dirname } = require('path')
const { writeFileSync, unlinkSync, readFileSync } = require('fs')
const userHome = require('user-home')
const findProcess = require('find-process')
const isRunning = require('is-running')
const mkdirp = require('mkdirp')

const title = 'commonshost'
const PIDFILE = join(userHome, `.${title}`, 'master.pid')
module.exports.PIDFILE = PIDFILE

const lock = module.exports.lock = () => {
  try {
    mkdirp.sync(dirname(PIDFILE))
    writeFileSync(PIDFILE, process.pid, { flag: 'wx+' })
  } catch (error) {
    if (error.code === 'EEXIST') {
      const pid = read()
      if (isRunning(pid)) {
        throw error
      } else {
        unlock()
        lock()
      }
    } else {
      throw error
    }
  }
}

const unlock = module.exports.unlock = () => {
  try {
    unlinkSync(PIDFILE)
  } catch (error) {
    if (error.code !== 'ENOENT') throw error
  }
}

const read = module.exports.read = () => {
  try {
    return parseInt(readFileSync(PIDFILE, 'utf8'), 10)
  } catch (error) {
    if (error.code !== 'ENOENT') throw error
  }
}

module.exports.matchByName = async (matchPid) => {
  return (await findProcess('name', title))
    .filter(({ name }) => name === title)
    .map(({ pid }) => parseInt(pid, 10))
    .filter((pid) => pid !== process.pid)
    .filter((pid) => pid === matchPid)
    .shift()
}
