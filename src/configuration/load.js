const { join, resolve, relative } = require('path')
const { fileExists } = require('../helpers/fileExists')
const { directoryExists } = require('../helpers/directoryExists')
const defaultsDeep = require('lodash.defaultsdeep')
const { defaultOptions } = require('./default/options')
const {
  ConfigurationValidator,
  normaliseHost,
  loadFile,
  findFile
} = require('@commonshost/configuration')
const { keygen } = require('tls-keygen')
const userHome = require('user-home')
const pino = require('pino')
const { staticDirectories } = require('@commonshost/static-directories')

async function load ({
  options = undefined,
  generateCertificate = false,
  serveDefaultSite = true,
  cwd = process.cwd(),
  configurationFilepath
} = {}) {
  const log = pino(options && options.log)
  let userOptions
  let defaultManifest
  if (typeof options === 'object') {
    userOptions = options
  } else if (typeof options === 'string' && options.length > 0) {
    const filepath = resolve(cwd, options)
    log.info(`Given configuration file: ${relative(cwd, filepath)}`)
    userOptions = await loadFile(filepath)
    configurationFilepath = filepath
  } else if (typeof options === 'undefined') {
    const filepath = await findFile(cwd)
    if (filepath !== undefined) {
      log.info(`Found configuration file: ${relative(cwd, filepath)}`)
      userOptions = await loadFile(filepath)
      configurationFilepath = filepath
    }
    if (userOptions === undefined) {
      userOptions = {}
      log.warn('Loading default server configuration')
      const manifest = 'serverpush.json'
      if (fileExists(resolve(cwd, manifest))) {
        defaultManifest = manifest
        configurationFilepath = join(cwd, 'configuration')
        log.warn(`Found server push manifest: ${relative(cwd, manifest)}`)
      }
    }
  } else {
    throw new Error('Invalid options type supplied')
  }

  const result = defaultsDeep(userOptions, defaultOptions)

  if (serveDefaultSite === true && result.hosts.length === 0) {
    const host = await normaliseHost()
    if (defaultManifest) {
      host.manifest = defaultManifest
    }
    result.hosts.push(host)
  }

  result.hosts = await Promise.all(result.hosts.map(async (host) => {
    let { domain, root } = host

    if (!domain) {
      domain = 'localhost'
    }

    if (root) {
      root = resolve(cwd, root)
    } else {
      for (const directory of staticDirectories) {
        const destination = join(cwd, directory)
        if (directoryExists(destination)) {
          root = destination
          log.info(`Static site found: ${relative(cwd, root)}`)
          break
        }
      }
      if (!root) {
        root = cwd
        log.info(`Defaulting to current directory: ${root}`)
      }
    }

    const overrides = { domain, root }
    const cloned = Object.assign({}, host, overrides)
    const options = { configurationFilepath, externalManifest: true }
    const normalised = await normaliseHost(cloned, options)
    return normalised
  }))

  if (process.env.PORT !== undefined) {
    const httpsPort = Number(process.env.PORT)
    result.https.port = httpsPort
    result.http.to = httpsPort
    const roundedThousand = httpsPort - (httpsPort % 1000)
    result.http.from = roundedThousand + 80
    if (result.http.from === result.https.port) {
      result.http.from = roundedThousand
    }
  }

  if (result.acme.redirect.endsWith('/')) {
    result.acme.redirect = result.acme.redirect.replace(/\/+$/g, '')
  }

  if (result.acme.webroot !== '') {
    result.acme.webroot = resolve(cwd, result.acme.webroot)
  }

  if (result.acme.store !== '') {
    result.acme.store = resolve(cwd, result.acme.store)
  }

  if (result.placeholder.hostNotFound !== '') {
    result.placeholder.hostNotFound =
      resolve(cwd, result.placeholder.hostNotFound)
  }

  if (result.https.key === '' && result.https.cert === '') {
    const key = join(userHome, '.commonshost/key.pem')
    const cert = join(userHome, '.commonshost/cert.pem')
    if (fileExists(key) && fileExists(cert)) {
      result.https.key = key
      result.https.cert = cert
    } else if (generateCertificate === true) {
      log.info(
        '🔐 Generating a private TLS certificate.\n' +
        '   Confirm to add as a trusted certificate to your key chain.'
      )
      await keygen({ key, cert })
      result.https.key = key
      result.https.cert = cert
    } else {
      throw new Error('Missing a private key & public certificate pair.')
    }
  }

  new ConfigurationValidator().validate(result)

  return result
}

module.exports.load = load
