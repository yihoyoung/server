module.exports.defaultOptions = {
  signature: true,
  via: {
    pseudonym: ''
  },
  acme: {
    proxy: '',
    redirect: '',
    store: '',
    key: '$store/$domain/key.pem',
    cert: '$store/$domain/cert.pem',
    webroot: ''
  },
  doh: false,
  placeholder: {
    hostNotFound: ''
  },
  log: {
    level: 'info'
  },
  workers: {
    count: 'max_physical_cpu_cores'
  },
  http: {
    redirect: true,
    from: 8080,
    to: 8443
  },
  https: {
    port: 8443,
    key: '',
    cert: '',
    ca: []
  },
  www: {
    redirect: false
  },
  hosts: []
}
