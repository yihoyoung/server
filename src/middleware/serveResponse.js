const Negotiator = require('negotiator')
const { createReadStream, stat } = require('fs')
const { promisify } = require('util')
const { buildHeaders } = require('../helpers/buildHeaders')
const { isLoopback } = require('../helpers/isLoopback')
const { serveDependencies } = require('./serveDependencies')
const { NotFound, InternalServerError } = require('http-errors')

module.exports.serveResponse = () => {
  return async function serveResponse (request, response, next) {
    const { resolved, headers } = buildHeaders(
      request.resolved,
      request.options.cacheControl.immutable,
      new Negotiator(request).encodings(),
      request.fileIndex
    )

    if (!isLoopback(request)) {
      headers['strict-transport-security'] =
        `max-age=${request.options.strictTransportSecurity.maxAge}`
    }

    const statusCode = response.statusCode || 200

    if (request.httpVersionMajor === 2) {
      request.once('error', next)
      response.once('error', next)
      response.stream.once('error', next)
      if (statusCode !== 200) {
        headers[':status'] = statusCode
      }
      response.stream.respondWithFile(
        resolved.absolute,
        Object.assign(response.getHeaders(), headers),
        {
          statCheck (stat, headers) {
            if (request.method === 'HEAD') {
              headers['content-length'] = stat.size
              response.stream.respond(headers)
              return false
            }
          },
          onError ({ code }) {
            if (code === 'ENOENT') {
              next(new NotFound())
            } else {
              next(new InternalServerError())
            }
          }
        }
      )
      try {
        await serveDependencies(request, response)
      } catch (error) {
        request.log.error(error)
      }
    } else {
      try {
        response.setHeader(
          'content-length',
          (await promisify(stat)(resolved.absolute)).size
        )
      } catch (error) {
        return next(error)
      }
      if (request.method === 'HEAD') {
        response.writeHead(statusCode, headers)
        response.flushHeaders()
        response.end()
      } else {
        const file = createReadStream(resolved.absolute)
        file.once('open', async () => {
          response.writeHead(statusCode, headers)
          file.pipe(response)
        })
        file.once('error', (error) => {
          file.removeAllListeners()
          if (!response.headersSent) next(error)
          else if (!response.finished) response.end()
        })
        file.once('end', () => {
          file.removeAllListeners()
        })
      }
    }
  }
}
