const { request: httpRequest } = require('http')
const send = require('send')
const { URL } = require('url')
const {
  InternalServerError,
  BadGateway,
  LoopDetected
} = require('http-errors')

const MOVED_PERMANENTLY = 301

module.exports.acmeChallenge = function acmeChallenge (options) {
  if (options.acme.proxy.length > 0) {
    return acmeProxy(options)
  } else if (options.acme.redirect.length > 0) {
    return acmeRedirect(options)
  } else if (options.acme.webroot.length > 0) {
    return acmeWebroot(options)
  } else {
    return passthrough(options)
  }
}

function acmeProxy (options) {
  return async function acmeChallengeProxy (request, response, next) {
    const url = options.acme.proxy + request.originalUrl
    const headers = {}
    if (options.via.pseudonym) {
      const intermediary = `${request.httpVersion} ${options.via.pseudonym}`
      const via = request.headers['via']
      if (via && via.includes(intermediary)) {
        next(new LoopDetected())
        return
      }
      headers['via'] = via === undefined
        ? intermediary
        : `${via}, ${intermediary}`
    }
    const { hostname, port, pathname: path } = new URL(url)
    const passthrough = httpRequest({ headers, hostname, port, path })
    passthrough.once('response', (upstream) => {
      response.writeHead(upstream.statusCode, upstream.headers)
      upstream.pipe(response, { end: true })
    })
    passthrough.once('error', () => {
      next(new BadGateway())
    })
    request.pipe(passthrough, { end: true })
  }
}

function acmeRedirect (options) {
  return async function acmeChallengeRedirect (request, response, next) {
    const location = options.acme.redirect + request.originalUrl
    response.writeHead(MOVED_PERMANENTLY, { location })
    response.end()
  }
}

function acmeWebroot (options) {
  return async function acmeChallengeWebroot (request, response, next) {
    const sendOptions = {
      root: options.acme.webroot,
      etag: false,
      lastModified: false
    }
    send(request, request.originalUrl, sendOptions)
      .on('error', (error) => {
        next(response.statusCode ? error : new InternalServerError())
      })
      .pipe(response)
  }
}

function passthrough (options) {
  return function acmeChallengePassthrough (request, response, next) {
    next()
  }
}
