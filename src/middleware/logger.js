const pino = require('pino')
const { TLSSocket } = require('tls')
const { getHost } = require('../helpers/getHost')

module.exports.logger = (options) => {
  const log = pino(options)
  return async function logger (request, response, next) {
    request.log = response.log = log
    const scheme = request.socket instanceof TLSSocket ? 'https' : 'http'
    const url = `${scheme}://${getHost(request)}${request.url}`
    log.info(`HTTP/${request.httpVersion} ${request.method} ${url}`)
    next()
  }
}
