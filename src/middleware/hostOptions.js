const { untilBefore } = require('until-before')
const { BadRequest, NotFound } = require('http-errors')
const { getHost } = require('../helpers/getHost')
const { indexFiles } = require('../helpers/indexFiles')
const { normaliseHost } = require('@commonshost/configuration')
const { redirect } = require('../helpers/redirect')

module.exports.hostOptions = (options, files) => {
  const optionsByDomain = new Map(options.hosts.map((host) => [
    host.domain,
    host
  ]))

  const filesByDomain = new Map()
  for (const domain of Object.keys(files)) {
    const { root, index } = files[domain]
    const { trailingSlash } = optionsByDomain.get(domain).directories
    const fileIndex = indexFiles(root, index, trailingSlash)
    filesByDomain.set(domain, fileIndex)
  }

  process.on('message', async (message) => {
    if (!message.domain || !message.type) return

    if (message.type === 'site-delete') {
      const index = options.hosts
        .findIndex(({ domain }) => domain === message.domain)
      if (index !== -1) {
        options.hosts.splice(index, 1)
      }
      optionsByDomain.delete(message.domain)
      filesByDomain.delete(message.domain)
      return
    }

    if (message.type === 'site-deploy' &&
      !('configuration' in message) &&
      !optionsByDomain.has(message.domain)
    ) {
      const options = { externalManifest: false }
      message.configuration = await normaliseHost({
        domain: message.domain,
        root: message.root
      }, options)
    }

    if (message.configuration) {
      const index = options.hosts
        .findIndex(({ domain }) => domain === message.domain)
      if (index === -1) {
        options.hosts.push(message.configuration)
      } else {
        options.hosts[index] = message.configuration
      }
      optionsByDomain.set(message.domain, message.configuration)
    }

    if (message.root && message.index) {
      const { trailingSlash } = optionsByDomain.get(message.domain).directories
      const fileIndex = indexFiles(message.root, message.index, trailingSlash)
      filesByDomain.set(message.domain, fileIndex)
    }
  })

  return function hostOptions (request, response, next) {
    const hostname = untilBefore.call(getHost(request), ':')
    if (hostname === '') {
      next(new BadRequest())
    } else if (filesByDomain.has(hostname) && optionsByDomain.has(hostname)) {
      request.fileIndex = filesByDomain.get(hostname)
      request.options = optionsByDomain.get(hostname)
      next()
    } else {
      if (options.www.redirect === true) {
        const hasWww = hostname.startsWith('www.')
        const withoutWww = hostname.substring(4)
        const withWww = `www.${hostname}`
        if (hasWww && optionsByDomain.has(withoutWww)) {
          return redirect(response, withoutWww, options.http.to, request.url)
        } else if (!hasWww && optionsByDomain.has(withWww)) {
          return redirect(response, withWww, options.http.to, request.url)
        }
      }
      next(new NotFound())
    }
  }
}
