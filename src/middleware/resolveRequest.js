const { format } = require('url')
const { extname, normalize } = require('path')
const parseUrl = require('parseurl')
const { getHost } = require('../helpers/getHost')
const { NotFound } = require('http-errors')
const accepts = require('accepts')

const PERMANENT_REDIRECT = 308

const fallbackExtensions = ['', '.html', '.htm']

module.exports.resolveRequest = () => {
  return function resolveRequest (request, response, next) {
    const url = parseUrl(request)
    const { pathname } = url

    const { fileIndex } = request
    if (fileIndex.pathnames.has(pathname)) {
      request.resolved = fileIndex.pathnames.get(pathname)
      return next()
    } else {
      const { trailingSlash } = request.options.directories
      const hasTrailingSlash = pathname.endsWith('/')

      let indexHtml
      if (trailingSlash === 'always' && !hasTrailingSlash) {
        indexHtml = `${pathname}/`
      } else if (trailingSlash === 'never' && hasTrailingSlash) {
        indexHtml = pathname.slice(0, -1)
      }

      if (indexHtml && fileIndex.pathnames.has(indexHtml)) {
        url.protocol = 'https'
        url.host = getHost(request)
        url.pathname = indexHtml
        response.writeHead(PERMANENT_REDIRECT, { location: format(url) })
        response.end()
        return
      }

      const fallback = request.options.fallback['200']
      if (fileIndex.absolute.has(fallback)) {
        if (
          hasTrailingSlash ||
          fallbackExtensions.includes(extname(pathname))
        ) {
          const accepted = accepts(request)
          if (accepted.type('text/html')) {
            request.resolved = fileIndex.absolute.get(fallback)
            return next()
          }
        }
      }

      const normalised = normalize(pathname)
      if (normalised !== pathname) {
        url.protocol = 'https'
        url.host = getHost(request)
        url.pathname = normalised
        response.writeHead(PERMANENT_REDIRECT, { location: format(url) })
        response.end()
        return
      }

      return next(new NotFound())
    }
  }
}
