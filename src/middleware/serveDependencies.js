const Negotiator = require('negotiator')
const { promisify } = require('util')
const { queue } = require('d3-queue')
const { buildHeaders } = require('../helpers/buildHeaders')
const {
  constants: {
    NGHTTP2_INTERNAL_ERROR: INTERNAL_ERROR,
    NGHTTP2_CANCEL: CANCEL
  }
} = require('http2')

module.exports.serveDependencies =
async function serveDependencies (request, response) {
  if (request.httpVersionMajor !== 2) {
    throw new Error(`Server Push unsupported with HTTP/${request.httpVersion}`)
  }
  if (!response.stream.pushAllowed ||
    !request.pushResponses ||
    request.pushResponses.length === 0
  ) {
    return
  }

  const pushQueue = queue(Math.min(
    response.stream.session.remoteSettings.maxConcurrentStreams,
    response.stream.session.localSettings.maxConcurrentStreams
  ) - 1)

  const immutablePatterns = request.options.cacheControl.immutable
  const supportedEncodings = new Negotiator(request).encodings()
  const fileIndex = request.fileIndex
  const isHeadResponse = request.method === 'HEAD'

  for (const { pushResponse, dependency } of request.pushResponses) {
    pushQueue.defer((callback) => {
      const { headers, resolved } = buildHeaders(
        dependency,
        immutablePatterns,
        supportedEncodings,
        fileIndex
      )
      pushResponse.respondWithFile(
        resolved.absolute,
        headers,
        {
          statCheck (stat, headers) {
            headers['content-length'] = stat.size
            if (isHeadResponse) {
              pushResponse.respond(headers, { endStream: true })
              return false
            }
          },
          onError ({ code }) {
            const reason = code === 'ENOENT' ? CANCEL : INTERNAL_ERROR
            pushResponse.close(reason)
          }
        }
      )
      pushResponse.on('error', () => callback())
      pushResponse.on('finish', callback)

      return {
        abort: () => {
          try {
            pushResponse.end()
          } catch (error) {
            request.log.error(error)
          }
        }
      }
    })
  }

  await promisify(pushQueue.awaitAll.bind(pushQueue))()
}
