const { promisify } = require('util')
const { createServer } = require('http')
const connect = require('connect')
const { fingerprint } = require('./middleware/fingerprint')
const { logger } = require('./middleware/logger')
const { allowedMethods } = require('./middleware/allowedMethods')
const { cdnLoopPrevention } = require('./middleware/cdnLoopPrevention')
const { acmeChallenge } = require('./middleware/acmeChallenge')
const { redirectHttps } = require('./middleware/redirectHttps')
const { errorHandler } = require('middleware-plain-error-handler')

const ACME_CHALLENGE = '/.well-known/acme-challenge/'

module.exports.redirect = async (options) => {
  const app = connect()
  app.use(fingerprint(options))
  app.use(logger(options))
  app.use(allowedMethods(['GET']))
  app.use(cdnLoopPrevention(options.via))
  app.use(ACME_CHALLENGE, acmeChallenge(options))
  app.use(redirectHttps(options))
  app.use(errorHandler())
  const server = createServer(app)
  await promisify(server.listen.bind(server))(options.http.from)
  return server
}
