const connect = require('connect')
const { fingerprint } = require('./middleware/fingerprint')
const { logger } = require('./middleware/logger')
const { playdoh } = require('playdoh')
const { hostOptions } = require('./middleware/hostOptions')
const { allowedMethods } = require('./middleware/allowedMethods')
const { serviceWorkerScope } = require('./middleware/serviceWorkerScope')
const { allowCors } = require('./middleware/allowCors')
const { cdnLoopPrevention } = require('./middleware/cdnLoopPrevention')
const { resolveRequest } = require('./middleware/resolveRequest')
const { resolveDependencies } = require('./middleware/resolveDependencies')
const { serveResponse } = require('./middleware/serveResponse')
const { serveFallback } = require('./middleware/serveFallback')
const { errorHandler } = require('middleware-plain-error-handler')

module.exports.app = (options, files) => {
  const app = connect()
  app.use(fingerprint(options))
  app.use(logger(options.log))
  if (options.doh !== false) {
    app.use(playdoh(options.doh))
  }
  app.use(hostOptions(options, files))
  app.use(allowedMethods(['GET', 'HEAD', 'OPTIONS']))
  app.use(serviceWorkerScope())
  app.use(allowCors())
  app.use(cdnLoopPrevention(options.via))
  app.use(resolveRequest())
  app.use(resolveDependencies())
  app.use(serveResponse(options))
  app.use(serveFallback(options))
  app.use(errorHandler())
  return app
}
