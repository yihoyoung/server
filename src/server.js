const { resolve } = require('path')
const { promisify } = require('util')
const { readFile, readFileSync } = require('fs')
const { app } = require('./app')
const ocsp = require('ocsp')
const { createSecureServer } = require('http2')
const { createSecureContext } = require('tls')
const isDomainName = require('is-domain-name')
const isIp = require('is-ip')

async function read (filepath) {
  const resolved = resolve(process.cwd(), filepath)
  return promisify(readFile)(resolved)
}

function readSync (filepath) {
  const resolved = resolve(process.cwd(), filepath)
  return readFileSync(resolved)
}

function stringTemplate (string, parameters) {
  if (typeof string !== 'string') {
    throw new TypeError(`First argument must be a string. Value: ${string}`)
  }
  return string.replace(
    /\$(\w+)/g,
    (match, key) => key in parameters ? parameters[key] : match
  )
}

module.exports.server = (options, files) => {
  const requestListener = app(options, files)

  const ecdhCurve = 'P-384:P-256'
  const fallbackKey = readSync(options.https.key)
  const fallbackCert = readSync(options.https.cert)
  const fallbackCa = options.https.ca.filter(String).map(readSync)

  function fallbackSecureContext () {
    return createSecureContext({
      ecdhCurve,
      key: fallbackKey,
      cert: fallbackCert,
      ca: fallbackCa
    })
  }

  const acmeCache = new Map()
  const serverOptions = {
    allowHTTP1: true,
    ecdhCurve,
    // key: fallbackKey,
    // cert: fallbackCert,
    // ca: fallbackCa,
    SNICallback: async (servername, callback) => {
      if (acmeCache.has(servername)) {
        const { key, cert } = acmeCache.get(servername)
        const contextOptions = { ecdhCurve, key, cert }
        return callback(null, createSecureContext(contextOptions))
      }

      if (!(isDomainName(servername) || isIp(servername))) {
        return callback(null, fallbackSecureContext())
      }

      try {
        const parameters = { store: options.acme.store, domain: servername }
        const keyPath = stringTemplate(options.acme.key, parameters)
        const certPath = stringTemplate(options.acme.cert, parameters)
        const [key, cert] = await Promise.all([
          read(resolve(options.acme.store, keyPath)),
          read(resolve(options.acme.store, certPath))
        ])
        const contextOptions = { ecdhCurve, key, cert }
        const context = createSecureContext(contextOptions)
        acmeCache.set(servername, { key, cert })
        return callback(null, context)
      } catch (error) {
        if (error.code !== 'ENOENT') {
          console.error(error)
        }
        return callback(null, fallbackSecureContext())
      }
    }
  }

  const server = createSecureServer(serverOptions, requestListener)

  const ocspCache = new ocsp.Cache()
  server.on('OCSPRequest', (certificate, issuer, callback) => {
    if (!issuer) return callback()
    ocsp.getOCSPURI(certificate, (error, uri) => {
      if (error) return callback(error)
      if (uri === null) return callback()
      const request = ocsp.request.generate(certificate, issuer)
      ocspCache.probe(request.id, (error, { response }) => {
        if (error) return callback(error)
        if (response) return callback(null, response)
        const options = {
          url: uri,
          ocsp: request.data
        }
        ocspCache.request(request.id, options, callback)
      })
    })
  })

  process.on('message', ({ type, domain }) => {
    if (type === 'certificate-issue' ||
      type === 'certificate-revoke' ||
      type === 'site-delete'
    ) {
      if (acmeCache.has(domain)) {
        acmeCache.delete(domain)
      }
    }
  })

  return server
}
