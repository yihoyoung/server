const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { hostname } = require('os')
const { h1, h2 } = require('./helpers/receive')

const fixture = {
  via: `${hostname()} (Foo Bar)`
}

let master

test('start server with Via header pseudonym', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    via: { pseudonym: fixture.via }
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Via header over encrypted HTTP/1', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await h1(url)
  t.is(response.status, 200)
  const expected = `1.1 ${fixture.via}`
  t.is(response.headers.get('via'), expected)
})

test('Via header over plaintext HTTP/1', async (t) => {
  const url = 'http://localhost:8080/'
  const response = await h1(url)
  const expected = `1.1 ${fixture.via}`
  t.is(response.headers.get('via'), expected)
})

test('Via header over HTTP/2', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await h2(url)
  t.is(response.headers.get(':status'), 200)
  const expected = `2.0 ${fixture.via}`
  t.is(response.headers.get('via'), expected)
})

test('Via header included on all responses including errors', async (t) => {
  const url = 'https://does-not-exist:8443/'
  const response = await h1(url)
  t.is(response.status, 404)
  const expected = `1.1 ${fixture.via}`
  t.is(response.headers.get('via'), expected)
})

test('Error when detecting a CDN loop', async (t) => {
  const url = 'https://localhost:8443/'
  const given = `2.0 ${fixture.via}`
  const response = await h2(url, { headers: { via: given } })
  const httpStatusLoopDetected = 508
  t.is(response.headers.get(':status'), httpStatusLoopDetected)
})

test('stop server', async (t) => master.close())

test('start server without Via header', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

test('Via header not set by default', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await h2(url)
  t.is(response.headers.get(':status'), 200)
  const expected = undefined
  t.is(response.headers.get('via'), expected)
})

test('stop server', async (t) => master.close())
