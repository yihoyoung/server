const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

const hostNotFound = join(
  __dirname,
  'fixtures/placeholder/host-not-found.html'
)

const doesNotExist = join(
  __dirname,
  'fixtures/placeholder/fixtures/does-not-exist'
)

const fixtures = [
  {
    scenario: 'Respond with placeholder',
    given: {
      options: { placeholder: { hostNotFound } },
      pathname: '/'
    },
    expected: {
      status: 404,
      contentType: 'text/html; charset=utf-8',
      body: 'Host Not Found Placeholder'
    }
  },
  {
    scenario: 'Request does not allow HTML',
    given: {
      options: { placeholder: { hostNotFound } },
      pathname: '/favicon.ico'
    },
    expected: {
      status: 404,
      contentType: 'text/plain; charset=utf-8',
      body: 'Not Found'
    }
  },
  {
    scenario: 'Incorrect placeholder',
    given: {
      options: { placeholder: { hostNotFound: doesNotExist } },
      pathname: '/'
    },
    expected: {
      status: 500,
      contentType: 'text/plain; charset=utf-8',
      body: 'Internal Server Error'
    }
  },
  {
    scenario: 'Default behaviour',
    given: {
      options: { placeholder: {} },
      pathname: '/'
    },
    expected: {
      status: 404,
      contentType: 'text/plain; charset=utf-8',
      body: 'Not Found'
    }
  }
]

for (const { scenario, given, expected } of fixtures) {
  let master
  test('start server', async (t) => {
    const cwd = join(__dirname, 'fixtures')
    master = new Master({ cwd, options: given.options })
    await master.listen()
  })

  test(`Request missing domain: ${scenario}`, async (t) => {
    const url = `https://foobar.localhost:8443${given.pathname}`
    const response = await h1(url)
    t.is(response.status, expected.status)
    t.is(response.headers.get('content-type'), expected.contentType)
    t.is(await response.text(), expected.body)
  })

  test('stop server', async (t) => master.close())
}
