const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

test('Resolve special URL paths safely', async (t) => {
  const url = 'https://localhost:8443/foo/../../bar//logo.svg'
  const response = await h1(url)
  t.is(response.status, 308)
  t.is(response.statusText, 'Permanent Redirect')
  t.is(
    response.headers.get('location'),
    'https://localhost:8443/bar/logo.svg'
  )
})

test('stop server', async (t) => master.close())
