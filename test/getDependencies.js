const test = require('blue-tape')
const { getDependencies } = require('../src/helpers/getDependencies')
const { normalise } = require('@commonshost/manifest')
const { indexFiles } = require('../src/helpers/indexFiles')
const { deepStrictEqual } = require('assert')

const PRIORITY_DEFAULT = 16
const PRIORITY_HIGH = 256
const PRIORITY_LOW = 1

const fileA = '/bla/index.html'
const fileB = '/bla/foo.js'
const fileC = '/bla/bar.js'
const fileD = '/bla/foo.js.map'
const fileE = '/bla/foo.js.gz'
const fileF = '/bla/foo.js.br'
const fileG = '/bla/foo.svg'
const fileH = '/bla/foo.css'

const root = '/bla'
const index = [fileA, fileB, fileC, fileD, fileE, fileF, fileG, fileH]
const trailingSlashAlways = 'always'

const fileIndex = indexFiles(root, index, trailingSlashAlways)

const fixtures = [
  {
    scenario: 'Nested dependencies',
    given: {
      entry: fileIndex.absolute.get(fileA).relative,
      files: fileIndex.relative,
      manifest: [
        { glob: '/*.html', push: '/foo.js' },
        { glob: '/foo.js', push: ['**/*', '!**/*.map'] }
      ]
    },
    expected: new Map([
      [fileIndex.absolute.get(fileB).relative, PRIORITY_DEFAULT],
      [fileIndex.absolute.get(fileC).relative, PRIORITY_DEFAULT],
      [fileIndex.absolute.get(fileG).relative, PRIORITY_DEFAULT],
      [fileIndex.absolute.get(fileH).relative, PRIORITY_DEFAULT]
    ])
  },
  {
    scenario: 'Custom priority',
    given: {
      entry: fileIndex.absolute.get(fileA).relative,
      files: fileIndex.relative,
      manifest: [
        {
          glob: '/index.html',
          push: [
            { glob: '**/*.js', priority: PRIORITY_HIGH },
            { glob: '**/*.css' },
            { glob: '**/*.svg', priority: PRIORITY_LOW }
          ]
        }
      ]
    },
    expected: new Map([
      [fileIndex.absolute.get(fileB).relative, PRIORITY_HIGH],
      [fileIndex.absolute.get(fileC).relative, PRIORITY_HIGH],
      [fileIndex.absolute.get(fileH).relative, PRIORITY_DEFAULT],
      [fileIndex.absolute.get(fileG).relative, PRIORITY_LOW]
    ])
  }
]

for (
  const {
    scenario,
    given: { manifest, files, entry },
    expected
  } of fixtures
) {
  test(scenario, async (t) => {
    const actual = getDependencies(
      normalise(manifest),
      files,
      entry
    )
    t.doesNotThrow(() => deepStrictEqual(actual, expected))
  })
}
