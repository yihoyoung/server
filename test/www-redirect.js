const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1, h2 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    www: { redirect: true },
    hosts: [
      { domain: 'example.net' },
      { domain: 'www.example.com' }
    ]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('Redirect HTTP/1 www. to root domain', async (t) => {
  const url = 'https://www.example.net:8443/foo'
  const response = await h1(url)
  t.is(response.status, 308)
  t.is(response.headers.get('location'), 'https://example.net:8443/foo')
})

test('Redirect HTTP/2 www. to root domain', async (t) => {
  const url = 'https://www.example.net:8443/foo'
  const response = await h2(url)
  t.is(response.headers.get(':status'), 308)
  t.is(response.headers.get('location'), 'https://example.net:8443/foo')
})

test('stop server', async (t) => master.close())
