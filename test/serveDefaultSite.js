const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

const fixtures = [
  {
    given: { serveDefaultSite: false },
    expected: 404
  },
  {
    given: { serveDefaultSite: true },
    expected: 200
  }
]

for (const { given, expected } of fixtures) {
  let master
  test('start server', async (t) => {
    const cwd = join(__dirname, 'fixtures')
    const options = {}
    master = new Master(Object.assign(
      { cwd, options },
      given
    ))
    await master.listen()
  })

  test('Default site on localhost', async (t) => {
    const url = 'https://localhost:8443/'
    const response = await h1(url)
    t.is(response.status, expected)
  })

  test('stop server', async (t) => master.close())
}
