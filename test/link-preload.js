const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const fetch = require('node-fetch')
const { Agent } = require('https')
const { domainToASCII: toASCII } = require('url')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      domain: toASCII('💩.localhost'),
      manifest: [{
        glob: '**/*.html',
        push: '**/*.{js,txt}'
      }]
    }]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

test('HTTP/1 with link preload over HTTPS', async (t) => {
  const url = `https://${toASCII('💩.localhost')}:8443/`
  const response = await fetch(url, {
    agent: new Agent({
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    })
  })

  t.is(response.status, 200)
  t.is(response.ok, true)
  t.is(
    decodeURIComponent(response.headers.get('link')),
    '</foo.💩.js>; rel=preload; as=script, ' +
    '</stuff.txt>; rel=preload; as='
  )
  const body = await response.text()
  t.ok(body.match(/Yay!/))
})

test('stop server', async (t) => master.close())
