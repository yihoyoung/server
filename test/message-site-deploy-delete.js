const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const tmp = require('tmp-promise')
const cpy = require('cpy')
const { h1: receive } = require('./helpers/receive')

const url = 'https://example.net:8443/'

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

test('Site not deployed', async (t) => {
  const response = await receive(url)
  t.is(response.status, 404)
})

let scratch
test('Deploy website', async (t) => {
  scratch = await tmp.dir({ unsafeCleanup: true })
  const cwd = join(__dirname, 'fixtures')
  await cpy('public/**/*', scratch.path, { cwd })
  const configuration = {
    domain: 'example.net',
    root: scratch.path
  }
  await master.message({
    type: 'site-deploy',
    domain: 'example.net',
    root: scratch.path,
    hasNewFiles: true,
    hasNewConfiguration: true,
    configuration
  })
})

test('Site deployed', async (t) => {
  const response = await receive(url)
  t.is(response.status, 200)
})

test('Delete website', async (t) => {
  await scratch.cleanup()
  await master.message({
    type: 'site-delete',
    domain: 'example.net',
    root: scratch.path
  })
})

test('Site deleted', async (t) => {
  const response = await receive(url)
  t.is(response.status, 404)
})

test('stop server', async (t) => master.close())
