const { connect } = require('http2')
const { URL } = require('url')
const { StringDecoder } = require('string_decoder')

class Response {
  constructor (headers, body, push) {
    this.headers = new Map(Object.entries(headers))
    this.body = body
    this.push = push
  }

  async text () {
    return this.body.length === 0
      ? ''
      : new StringDecoder().end(this.body)
  }

  async json () {
    return JSON.parse(await this.text())
  }

  async arrayBuffer () {
    return this.body
  }
}

function receiveHttp2 (url, options = {}) {
  return new Promise((resolve, reject) => {
    const pending = new Set()
    let result
    const push = []
    const done = (stream, payload) => {
      if (pending.has(stream)) {
        pending.delete(stream)
        if (payload) {
          push.push(payload)
        }
        if (pending.size === 0) {
          session.close(() => {
            resolve(new Response(result.response, result.data, push))
          })
        }
      }
    }

    const session = connect(url, Object.assign({
      rejectUnauthorized: false,
      lookup: (hostname, options, callback) => {
        callback(null, '127.0.0.1', 4)
      }
    }, options))
    const headers = Object.assign(
      { ':path': new URL(url).pathname },
      options.headers
    )
    const request = session.request(headers)
    session.on('stream', (stream, request, flags) => {
      pending.add(stream)
      const chunks = []
      stream.on('data', (chunk) => chunks.push(chunk))
      stream.on('push', (response, flags) => {
        stream.on('close', () => {
          const data = Buffer.concat(chunks)
          done(stream, { request, response, data })
        })
      })
      stream.on('aborted', () => {
        if (stream.rstCode === 8) {
          done(stream, { request, rstCode: stream.rstCode })
        }
      })
      stream.on('error', (error) => {
        done(stream, { request, error })
      })
    })
    request.on('response', (response, flags) => {
      pending.add(response)
      const chunks = []
      request.on('data', (chunk) => chunks.push(chunk))
      request.on('close', () => {
        const data = Buffer.concat(chunks)
        result = { response, data }
        done(response)
      })
    })
    request.end(options.body)
  })
}

module.exports.receiveHttp2 = receiveHttp2
