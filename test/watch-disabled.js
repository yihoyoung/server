const test = require('blue-tape')
const { Master } = require('..')
const { join, basename } = require('path')
const { h2: receive } = require('./helpers/receive')
const tmp = require('tmp-promise')
const {
  constants: {
    NGHTTP2_CANCEL: CANCEL
  }
} = require('http2')

// tmp.setGracefulCleanup()

let file

test('Add a new file', async (t) => {
  file = await tmp.file({
    dir: join(__dirname, 'fixtures/public'),
    prefix: 'temp-',
    postfix: '.js'
  })
})

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [{
      domain: 'localhost',
      manifest: [{ get: '**/*.html', push: '**/*.js' }]
    }]
  }
  master = new Master({ cwd, options, watch: false })
  await master.listen()
})

const url = 'https://localhost:8443'
const options = { rejectUnauthorized: false }

test('Normal response and push promise', async (t) => {
  const response = await receive(url, options)
  t.is(response.headers.get(':status'), 200)
  t.is(response.push.length, 2)
  t.ok(response.push.find(({ request }) => {
    return request[':path'] === encodeURI('/foo.💩.js')
  }))
  t.ok(response.push.find(({ request }) => {
    return request[':path'] === encodeURI(`/${basename(file.path)}`)
  }))
})

test('cleanup temp file', async (t) => file.cleanup())

test('Updated push promises', async (t) => {
  const response = await receive(url, options)
  t.is(response.headers.get(':status'), 200)
  t.is(response.push.length, 2)
  t.ok(response.push.find(({ request }) => {
    return request[':path'] === encodeURI('/foo.💩.js')
  }))
  const cancelled = response.push.find(({ request }) => {
    return request[':path'] === encodeURI(`/${basename(file.path)}`)
  })
  t.ok(cancelled)
  t.is(cancelled.rstCode, CANCEL)
})

test('Missing response', async (t) => {
  const urlMissing = `${url}/${encodeURI(basename(file.path))}`
  const response = await receive(urlMissing, options)
  t.is(response.headers.get(':status'), 404)
  t.is(response.push.length, 0)
})

test('stop server', async (t) => master.close())
