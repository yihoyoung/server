const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { h1 } = require('./helpers/receive')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures/public')
  const options = undefined
  master = new Master({ cwd, options })
  await master.listen()
})

test('Default root is cwd', async (t) => {
  const url = 'https://localhost:8443/'
  const response = await h1(url)
  t.is(response.status, 200)
})

test('stop server', async (t) => master.close())
