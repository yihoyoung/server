const { join } = require('path')

module.exports = {
  workers: {
    count: 'max_physical_cpu_cores / 2'
  },
  acme: {
    redirect: 'http://localhost:12345/',
    store: join(__dirname, 'acme/store'),
    key: '$store/$domain/key.pem',
    cert: '$store/$domain/key.pem',
    webroot: join(__dirname, 'acme/webroot')
  },
  hosts: [
    {
      domain: 'localhost',
      root: join(__dirname, 'public'),
      fallback: {
        200: '/200.html',
        404: '/404.html'
      },
      cacheControl: {
        immutable: [
          'hex',
          'emoji'
        ]
      },
      manifest: [
        {
          glob: '**/*.html',
          push: [
            {
              glob: [
                '**/*',
                '!**/*.{map,html}',
                '!**/service-worker/**/*'
              ],
              priority: 256
            }
          ]
        }
      ]
    }
  ]
}
