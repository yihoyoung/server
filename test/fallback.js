const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('http2')
const { StringDecoder } = require('string_decoder')
const concat = require('concat-stream')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {
    hosts: [
      {
        domain: 'fallback-200',
        fallback: { 200: '/200.html' }
      },
      {
        domain: 'fallback-404',
        fallback: { 404: '/404.html' }
      }
    ]
  }
  master = new Master({ cwd, options })
  await master.listen()
})

const fixtures = {
  200: new Map([
    [
      'Serve fallback on missing extension',
      { path: '/foobar', status: 200, payload: 'Fallback found' }
    ],
    [
      'Serve fallback on HTML file',
      { path: '/foobar.html', status: 200, payload: 'Fallback found' }
    ],
    [
      'Serve fallback on directory',
      { path: '/foobar', status: 200, payload: 'Fallback found' }
    ],
    [
      'Serve 404 error on generic file extension',
      { path: '/foo.bar', status: 404, payload: 'Not Found' }
    ]
  ]),
  404: new Map([
    [
      'Serve 404 fallback on missing extension',
      { path: '/foobar', status: 404, payload: 'Fallback missing' }
    ],
    [
      'Serve 404 fallback on HTML file',
      { path: '/foobar.html', status: 404, payload: 'Fallback missing' }
    ],
    [
      'Serve 404 fallback on directory',
      { path: '/foobar', status: 404, payload: 'Fallback missing' }
    ],
    [
      'Serve 404 fallback on generic file extension',
      { path: '/foo.bar', status: 404, payload: 'Fallback missing' }
    ]
  ])
}

for (const key of Object.keys(fixtures)) {
  for (const [label, { path, status, payload }] of fixtures[key]) {
    test(label, (t) => {
      const session = connect(
        `https://fallback-${key}:8443`,
        {
          rejectUnauthorized: false,
          lookup: (hostname, options, callback) => {
            callback(null, '127.0.0.1', 4)
          }
        }
      )

      const stream = session.request({ ':path': path })
      stream.on('socketError', t.end)
      stream.on('error', t.end)

      stream.on('response', (headers) => {
        t.is(headers[':status'], status)
        // stream.resume()
        // stream.on('end', () => stream.close())
        stream.pipe(concat((data) => {
          const body = new StringDecoder().end(data)
          t.is(body, payload)
          stream.close()
        }))
        stream.on('close', () => session.close(t.end))
      })
    })
  }
}

test('stop server', async (t) => master.close())
