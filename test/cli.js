const test = require('blue-tape')
const { startServer } = require('./helpers/startServer')
const eventToPromise = require('event-to-promise')

let server
test('Start server', async (t) => {
  const args = ['start']
  const message = 'Server started'
  server = await startServer({ args, message })
})

test('Reload server', async (t) => {
  const args = ['reload']
  const message = 'Reloading process'
  const node = await startServer({ args, message })
  await eventToPromise(node, 'exit')
})

test('Stop server', async (t) => {
  const args = ['stop']
  const message = 'Stopping process'
  const node = await startServer({ args, message })
  await Promise.all([
    eventToPromise(node, 'exit'),
    eventToPromise(server, 'exit')
  ])
})
