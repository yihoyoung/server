const test = require('blue-tape')
const { cacheDigestFilter } = require('../src/helpers/cacheDigestFilter')

const fixtures = [
  {
    scenario: 'No digest present',
    request: { headers: {} },
    cookies: { get: () => undefined },
    baseUrl: 'https://example.com',
    given: [
      '/style.css'
    ],
    expected: [
      '/style.css'
    ]
  },

  {
    scenario: 'Digest present in header',
    request: { headers: { 'cache-digest': 'AfdA; complete' } },
    cookies: { get: () => undefined },
    baseUrl: 'https://example.com',
    given: [
      '/style.css',
      '/not_in_the_digest'
    ],
    expected: [
      '/not_in_the_digest'
    ]
  },

  {
    scenario: 'Digest present in cookie',
    request: { headers: {} },
    cookies: { get: () => 'AfdA' },
    baseUrl: 'https://example.com',
    given: [
      '/style.css',
      '/not_in_the_digest'
    ],
    expected: [
      '/not_in_the_digest'
    ]
  },

  {
    scenario: 'Input should be unescaped URL',
    request: { headers: { 'cache-digest': 'Aqqg; complete' } },
    cookies: { get: () => undefined },
    baseUrl: 'https://example.com',
    given: [
      '/💩.js',
      decodeURI('/%F0%9F%92%A9.js'),
      '/not_in_the_digest'
    ],
    expected: [
      '/not_in_the_digest'
    ]
  }
]

for (const fixture of fixtures) {
  const { scenario, request, cookies, baseUrl, given, expected } = fixture
  test(scenario, async (t) => {
    const actual = given.filter(
      cacheDigestFilter(request, cookies, baseUrl)
    )
    t.deepEqual(actual, expected)
  })
}
