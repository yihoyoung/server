const test = require('blue-tape')
const { Master } = require('..')
const { join } = require('path')
const { connect } = require('http2')
const { Agent, request } = require('https')
const { StringDecoder } = require('string_decoder')
const concat = require('concat-stream')
const { PassThrough: identity } = require('stream')
const { createGunzip: gzip } = require('zlib')
const { decompressStream: brotli } = require('iltorb')

let master
test('start server', async (t) => {
  const cwd = join(__dirname, 'fixtures')
  const options = {}
  master = new Master({ cwd, options })
  await master.listen()
})

const fixtures = [
  {
    label: 'GET with GZip',
    method: 'GET',
    encoding: {
      request: 'gzip',
      response: 'gzip'
    },
    length: {
      header: 26,
      payload: 26
    },
    payload: 'Blablabla\n',
    decoder: gzip
  },
  {
    label: 'GET with Brotli',
    method: 'GET',
    encoding: {
      request: 'br',
      response: 'br'
    },
    length: {
      header: 14,
      payload: 14
    },
    payload: 'Blablabla\n',
    decoder: brotli
  },
  {
    label: 'HEAD without compression',
    method: 'HEAD',
    encoding: {
      request: 'identity',
      response: undefined
    },
    length: {
      header: 10,
      payload: 0
    },
    payload: '',
    decoder: identity
  },
  {
    label: 'HEAD with brotli',
    method: 'HEAD',
    encoding: {
      request: 'br',
      response: 'br'
    },
    length: {
      header: 14,
      payload: 0
    },
    payload: '',
    decoder: brotli
  },
  {
    label: 'server prefers Brotli encoding',
    method: 'GET',
    encoding: {
      request: 'identity, gzip, br',
      response: 'br'
    },
    length: {
      header: 14,
      payload: 14
    },
    payload: 'Blablabla\n',
    decoder: brotli
  },
  {
    label: 'server prefers GZip encoding',
    method: 'GET',
    encoding: {
      request: 'identity, gzip',
      response: 'gzip'
    },
    length: {
      header: 26,
      payload: 26
    },
    payload: 'Blablabla\n',
    decoder: gzip
  }
]

for (const { label, method, encoding, length, payload, decoder } of fixtures) {
  test(`HTTP/2 ${label}`, (t) => {
    const session = connect(
      'https://localhost:8443',
      { rejectUnauthorized: false }
    )

    const stream = session.request({
      ':method': method,
      ':path': '/stuff.txt',
      'accept-encoding': encoding.request
    })

    stream.on('response', (headers) => {
      t.is(headers[':status'], 200)
      t.is(headers['content-encoding'], encoding.response)
      t.is(headers['content-length'], String(length.header))

      stream
        .pipe(concat((data) => {
          t.is(data.length, length.payload)
        }))

      stream
        .pipe(decoder())
        .pipe(concat((data) => {
          const body = data.length === 0 ? '' : new StringDecoder().end(data)
          t.is(body, payload)
          session.close(t.end)
        }))
    })

    session.on('socketError', t.end)
    session.on('error', t.end)
    stream.on('error', t.end)
  })

  test(`HTTP/1 ${label}`, (t) => {
    const options = {
      headers: { 'accept-encoding': encoding.request },
      method: method,
      port: 8443,
      path: '/stuff.txt',
      agent: new Agent({ rejectUnauthorized: false })
    }
    const client = request(options, async (response) => {
      t.is(response.headers['content-length'], String(length.header))
      t.is(response.headers['content-encoding'], encoding.response)

      response
        .pipe(concat((data) => {
          t.is(data.length, length.payload)
        }))

      response
        .pipe(decoder())
        .pipe(concat((data) => {
          const body = data.length === 0 ? '' : new StringDecoder().end(data)
          t.is(body, payload)
        }))

      response.on('end', t.end)
    })
    client.end()
  })
}

test('stop server', async (t) => master.close())
